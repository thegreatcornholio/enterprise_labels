import sqlite3
import domain.entities as m


class SqliteRepo(m.RepoInterface):
    def __init__(self, connection: sqlite3.Connection):
        self.connection: sqlite3.Connection = connection
        self.ddl = """
            CREATE TABLE IF NOT EXISTS servers (
                     uid INTEGER PRIMARY KEY,
               label_uid INTEGER
            );

            CREATE TABLE IF NOT EXISTS labels (
                     uid INTEGER PRIMARY KEY,
                category STRING NOT NULL,
         in_category_uid INTEGER NOT NULL,
                 is_free INTEGER NOT NULL -- 1 (free), 0 (used), -1 (taken to be used)
            );
        """
        self.connection.executescript(self.ddl)

    def new_server(self) -> m.Server:
        query = ' INSERT INTO servers (label_uid) VALUES (NULL) '
        uid = self.connection.execute(query).lastrowid
        return m.Server(uid=uid)

    def get_server_by_uid(self, uid: int) -> m.Server:
        query = """
            SELECT s.uid
                 , l.in_category_uid
                 , l.category
              FROM servers s
         LEFT JOIN labels l
                ON s.label_uid = l.uid
             WHERE s.uid = ?
        """
        match self.connection.execute(query, (uid, )).fetchone():
            case server_uid, None, None:
                return m.Server(uid=server_uid)

            case server_uid, label_cat_uid, category:
                label = m.Label(in_category_uid=label_cat_uid, category=m.Category[category])
                return m.Server(uid=server_uid, label=label)

            case _:
                raise Exception(f'no server with {uid=}')

    def get_first_free_label(self, category: m.Category) -> m.Label | None:
        query = ' SELECT MIN(in_category_uid) FROM labels WHERE category = ? AND is_free = 1 '
        uid = self.connection.execute(query, (category.name, )).fetchone()[0]

        if uid:
            # TODO: should label be marked here as taken to be used?
            return m.Label(uid, category)
        else:
            return None

    def create_label(self, category: m.Category) -> m.Label:
        query = ' SELECT MAX(in_category_uid) FROM labels WHERE category = ? '
        in_cat_uid = self.connection.execute(query, (category.name,)).fetchone()[0]

        if in_cat_uid is None:
            in_cat_uid = 0

        query = ' INSERT INTO labels (category, in_category_uid, is_free) VALUES (?, ?, 1) '
        self.connection.execute(query, (category.name, in_cat_uid + 1, ))
        return m.Label(in_cat_uid + 1, category)

    def assign_label(self, server: m.Server, label: m.Label) -> m.Server:
        label_uid = self.connection.execute(
            ' SELECT uid FROM labels WHERE category = ? AND in_category_uid = ? ',
            (label.category.name, label.in_category_uid, )
        ).fetchone()[0]

        assert label_uid

        self.connection.execute(
            ' UPDATE servers SET label_uid = ? WHERE uid = ? ',
            (label_uid, server.uid, )
        )

        self.connection.execute(
            ' UPDATE labels SET is_free = 0 WHERE uid = ? ',
            (label_uid, )
        )

        return self.get_server_by_uid(server.uid)

    def remove_label(self, server: m.Server) -> m.Server:
        label_uid = self.connection.execute(
            ' SELECT label_uid FROM servers WHERE uid = ? ',
            (server.uid, )
        ).fetchone()[0]

        if label_uid:
            self.connection.execute(' UPDATE servers SET label_uid = NULL WHERE uid = ? ', (server.uid, ))
            self.connection.execute(' UPDATE labels SET is_free = 1 WHERE uid = ? ', (label_uid, ))

        return self.get_server_by_uid(server.uid)

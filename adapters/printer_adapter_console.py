import domain.entities as m


class ConsolePrinter(m.PrinterInterface):
    def __init__(self):
        self.log = []
        self._in_transaction = False

    def print_label(self, label: m.Label):
        self._in_transaction = True
        print(label.value)
        self.log.append(label.value)

    def commit(self):
        self._in_transaction = False

    def rollback(self):
        if self._in_transaction:
            self.log.append("don't use just printed label")
            self._in_transaction = False

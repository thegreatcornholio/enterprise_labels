""" repo implementation with Python native types and data structures """
from queue import PriorityQueue
from typing import Generator
import domain.entities as m


class DumbConnection(m.Connection):
    def commit(self):
        pass

    def rollback(self):
        pass


def uid_gen():
    """ uid generator for labels """
    i = 0
    while True:
        yield i
        i += 1


class MemoryRepo(m.RepoInterface):
    def __init__(self, connection: DumbConnection):
        self.connection = connection
        self.servers: list[m.Server] = []
        self._in_category_uid: dict[str, Generator] = {c.name: uid_gen() for c in m.Category}
        self.free_labels: dict[str, PriorityQueue] = {c.name: PriorityQueue() for c in m.Category}

    def new_server(self) -> m.Server:
        server = m.Server(len(self.servers))
        self.servers.append(server)
        return server

    def get_server_by_uid(self, uid: int) -> m.Server:
        return self.servers[uid]

    def get_first_free_label(self, category: m.Category) -> m.Label | None:
        if self.free_labels[category.name].empty():
            return None
        else:
            _, label = self.free_labels[category.name].get()
            return label

    def create_label(self, category: m.Category) -> m.Label:
        in_cat_uid = next(self._in_category_uid[category.name])
        label = m.Label(in_cat_uid, category)
        self.free_labels[category.name].put((in_cat_uid, label))
        return label

    def assign_label(self, server: m.Server, label: m.Label) -> m.Server:
        # TODO: not free label can be assigned here; same label can be assigned more than 1 time
        # TODO: where should data invariants be checked?
        server.label = label
        return server

    def remove_label(self, server: m.Server):
        if server.label:
            self.free_labels[server.label.category.name].put(
                (server.label.in_category_uid, server.label)
            )
            server.label = None

        return server

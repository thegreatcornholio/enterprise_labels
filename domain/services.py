""" business logic """

from domain import entities as m


def get_label(category: m.Category, repo: m.RepoInterface, printer: m.PrinterInterface) -> m.Label:
    label = repo.get_first_free_label(category)

    # TODO: should it be moved to the separate method?
    if label is None:
        label = repo.create_label(category)
        printer.print_label(label)

    return label


def stick_label(server: m.Server, label: m.Label, repo: m.RepoInterface) -> m.Server:
    server = repo.assign_label(server, label)
    return server


def unstick_label(server: m.Server, repo: m.RepoInterface) -> m.Server:
    server = repo.remove_label(server)
    return server

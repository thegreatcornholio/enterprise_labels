""" domain model/entities/core/... and interfaces for infrastructure """

from enum import Enum
from dataclasses import dataclass
from typing import Optional, Protocol


class Category(Enum):
    """ Server's Category """
    RED = 1
    GREEN = 2
    BLUE = 3


@dataclass
class Label:
    """ Category and number is printed on each label. """
    # TODO: is it a value-object?
    # TODO: can uid be here if there is no uid in the business logic?
    in_category_uid: int
    category: Category

    @property
    def value(self):
        return f'{self.category.name}:{self.in_category_uid}'


@dataclass
class Server:
    uid: int
    label: Optional[Label] = None


class Connection(Protocol):
    """ interface for database connection """

    def commit(self):
        pass

    def rollback(self):
        pass


class RepoInterface(Protocol):
    """ interface for the data repository """
    # TODO: create a repo for the aggregate instead of this one with all the checks inside

    connection: Connection

    # TODO: should there be a separate repo for each entity?
    # TODO: should methods be CRUD-like?

    def new_server(self) -> Server:
        """ create new server """

    def get_server_by_uid(self, uid: int) -> Server:
        """ get server by uid """

    def get_first_free_label(self, category: Category) -> Label | None:
        """ get free label with minimum uid, if any """

    def create_label(self, category: Category) -> Label:
        """ create new Label """

    def assign_label(self, server: Server, label: Label) -> Server:
        """ assign server a label """

    def remove_label(self, server: Server) -> Server:
        """ remove label from server """


class PrinterInterface(Protocol):
    """ interface for printer device """
    log: list[str]

    def print_label(self, label: Label):
        """ print a label """

    # TODO: is it a correct place for commit/rollback?
    def commit(self):
        """ is called when just label is ready to use """

    def rollback(self):
        """ do something if just printed label should not be used """

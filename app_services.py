""" entrypoints to the app """
import entities as m
from domain import entities as m, services as b


class UnitOfWork:
    def __init__(self, repo: m.RepoInterface, printer: m.PrinterInterface = None):
        self.repo = repo
        self.printer = printer

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            self.rollback()
        else:
            self.commit()

    def commit(self):
        self.repo.connection.commit()

        if self.printer:
            self.printer.commit()

    def rollback(self):
        self.repo.connection.rollback()

        if self.printer:
            self.printer.rollback()


def add_server_to_category(server_uid: int, category: str, uow: UnitOfWork) -> m.Server:
    with uow:
        server = uow.repo.get_server_by_uid(server_uid)
        server = b.unstick_label(server, uow.repo)
        label = b.get_label(m.Category[category], uow.repo, uow.printer)
        server = b.stick_label(server, label, uow.repo)

    # TODO: should it return entities or primitive types?
    return server


def remove_server_from_category(server_uid: int, uow: UnitOfWork) -> m.Server:
    with uow:
        server = uow.repo.get_server_by_uid(server_uid)
        server = b.unstick_label(server, uow.repo)

    return server

import pytest
import sqlite3
from adapters import repo_adapter_sqlite as s
from adapters import printer_adapter_console as p
import app_services as app


@pytest.fixture(scope='function')
def connection(tmp_path_factory):
    db = tmp_path_factory.mktemp('tests') / 'db_sqlite.db'
    conn = sqlite3.connect(db)
    yield conn
    conn.close()


@pytest.fixture(scope='function')
def uow(connection):
    yield app.UnitOfWork(s.SqliteRepo(connection), p.ConsolePrinter())


def test_adding_and_removing_from_category(uow):
    server = uow.repo.new_server()
    category = 'RED'

    # assign server a new label
    server = app.add_server_to_category(server.uid, category, uow)
    assert server.label.category.name == category

    # remove from category
    server = app.remove_server_from_category(server.uid, uow)
    assert server.label is None


def test_changing_categories(uow):
    server = uow.repo.new_server()
    category1 = 'RED'
    category2 = 'GREEN'

    # add server to the first category
    server = app.add_server_to_category(server.uid, category1, uow)
    assert server.label.category.name == category1

    # add to the other category
    server = app.add_server_to_category(server.uid, category2, uow)
    assert server.label.category.name == category2

""" tests on domain services """
import pytest
from adapters import repo_adapter_python as ar
from adapters import printer_adapter_console as ap
from domain import services as s
from domain import entities as m


@pytest.fixture(scope='function')
def repo():
    yield ar.MemoryRepo(ar.DumbConnection())


@pytest.fixture(scope='function')
def printer():
    yield ap.ConsolePrinter()


def test_print_new_label(repo, printer):
    """ test printing new label, getting free label and printer log """
    category = m.Category['RED']
    label1 = s.get_label(category, repo, printer)
    label2 = s.get_label(category, repo, printer)

    assert label1.category == category
    assert label1.value == f'{category.name}:0'
    assert printer.log[-1] == label1.value
    assert label1 == label2


def test_id_in_category_is_minimal(repo, printer):
    """ test that label with minimum in_category_id is returned by get_label """
    label1 = repo.create_label(m.Category['RED'])
    label2 = repo.create_label(m.Category['RED'])
    label3 = repo.create_label(m.Category['RED'])

    server = repo.new_server()
    label1_ = s.get_label(m.Category['RED'], repo, printer)
    server = s.stick_label(server, label1_, repo)
    assert server.label == label1

    label = s.get_label(m.Category['RED'], repo, printer)
    assert label == label2
